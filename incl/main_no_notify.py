import socket
import socks
from queue import Queue
from threading import Thread
import os
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import binascii
from time import sleep
from subprocess import call
import readline
import time
import subprocess
import sys


#####################################
#### Tor Chat By Holistic Coding ####
####   SoftExploit and Korrupt   #### 
#####################################



## Color Coding
class BC:
    G = '\033[92m'
    A = '\033[0;37m'
    F = '\033[91m'
    B = '\033[1m'
    E = '\033[0m'

## Main Function
class Torchat:

    def __init__(self):
        self.socks_port = 9050
        self.torme = input("Use Tor on %s? [y/n]: " % self.socks_port)
        self.addy = input("Address: ")
        self.port = input("Port: ")
        self.questioning()
        
    ## Tor
    def tor(self):
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", self.socks_port, True)
        self.s = socks.socksocket()
        self.s.connect((self.addy, int(self.port)))
    
    ## Clearnet
    def clearnet(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((self.addy, int(self.port)))

    ## Initial Questions
    def questioning(self):
        if self.torme.lower() == "yes" or self.torme.lower() == "y":
            self.tor()
        else:
            self.clearnet()
        ThreadCount = 0
        self.q = Queue()
        self.name = input("Enter Your Nick: ")
        self.password = input("Enter Your Password: ")
        print('> Locked And Loaded.')
        print('> Waiting For Connections...')
        self.data = "" 

    ## Timestamp
    def timed(self):
        self.secondsSinceEpoch = time.time()
        self.timeObj = time.localtime(self.secondsSinceEpoch)
        self.timer = "[" + BC.F + str(self.timeObj.tm_hour) + BC.E + ":" + BC.F + str(self.timeObj.tm_min) + BC.E + ":"  + BC.F + str(self.timeObj.tm_sec) + BC.E + "]"   
       
    ## Encrypt Messages
    def encrypt(self, msg):
        encryptor = PKCS1_OAEP.new(self.pubKey)
        self.encrypted = encryptor.encrypt(msg)
        encrypt = binascii.hexlify(self.encrypted)
        self.encrypted = encrypt.decode()

    ## Decrypt Messages
    def decrypt(self, mssg):
        try:
            msg = binascii.unhexlify(mssg)
            self.decryptor = PKCS1_OAEP.new(self.privKeyPEM)
            self.decrypted = self.decryptor.decrypt(msg)
            return self.decrypted.decode()
        except ValueError:
            pass

    ## Get keys
    def keys(self):
        subprocess.run(["touch", "incl/public.pem"])
        subprocess.run(["touch", "incl/private.pem"])
        subprocess.run(["touch", "incl/privated.pem"])
        with open('incl/public.pem') as f:
            self.pkey = f.read()
        if self.pkey == "":
            pass
        else:
            self.pubKey = RSA.importKey(self.pkey)
        with open('incl/privated.pem') as f:
            self.mykey = f.read()
        with open('incl/private.pem') as f:
            self.prkey = f.read()
        if self.prkey == "":
            pass
        try:
            self.privKeyPEM = RSA.importKey(self.prkey)
            print(self.privKeyPEM)
        except ValueError:
            pass
         
    ## Initial start up
    def start(self):
        self.keys()
        self.initing()
    
    ## Send message to be encrypted, then send message
    def sending(self, jj):
        self.encrypt(jj.encode('utf-8'))
        try:
            self.s.send(self.encrypted.encode('utf-8'))
        except OSError:
            sys.exit(1)

    ## Main User Function
    ## For communication from the server
    def threaded_user(self, connection):
        self.data
        sleep(.5)
        self.enter = BC.F + self.name + BC.E + " has connected."
        self.sending(self.enter)
        while True:
            try:
    ## Receive Data From Server
                self.data = connection.recv(2048)
            except OSError:
                sys.exit(0)
                return
            check = self.data.decode('utf-8')
    ## Various communication from server
            if check[:4] == "User":
                print(check)
    ## Private messages
            elif check[:1] == "@":
                a = check.split()[0]
                check = check.replace(a, '')
                self.decrypt(check[1:])
                check = a + self.decrypted.decode('utf-8')
                print(check)
    ## Current Users
            elif check[:7] == "Current":
                print(check)
    ## Errors
            elif check[:5] == "Error":
                print(check)
                print("Connection Terminated.")
                subprocess.run(['pkill', 'chat.py'])
    ## Password Check
            elif check[:8] == "Password":
                self.ms = input("Enter Password: ")
                self.ms = "Password " + self.ms
                self.s.send(self.ms.encode('utf-8'))
                print("Type !help for commands.")
    ## List Admins
            elif check[:6] == "Admins":
                print(check)
    ## Kicked Notice
            elif check == "You have been kicked!":
                print(check)
                self.s.close()
                subprocess.run(['pkill', 'chat.py'])
            else:
                try:
    ## Decrypt messages, then display
                    self.decrypted = self.decrypt(self.data.decode('utf-8'))
                    if self.decrypted == None:
                        if self.name in self.data.decode('utf-8'):
                            print(self.data.decode('utf-8'))
                        else:
                            print(self.data.decode('utf-8'))
                    else:
                        if self.name in self.decrypted:
                            self.parse = self.decrypted.replace('[0m', '')
                            self.parse = self.parse.replace('[92m', '')
                            self.timed()
                            print(self.timer + " " + self.decrypted)
                        else:
                            self.timed()
                            print(self.timer + " " + self.decrypted)
                except NameError:
                    pass   
                except OSError:
                    sys.exit(0)
    
    ## Main function
    def main(self):
        try:
    ## Input area
            self.ms = input("")
        except (ValueError, OSError):
            sys.exit(0)
        sys.stdout.write("\033[F")
        self.timed()
        print(self.timer + " " + BC.F + self.name + BC.E +  ": " + self.ms )
    ## Character Cap
        if len(self.ms) >= 240:
            print(BC.F + "Your message must be below 200 characters. Not sent." + BC.E)
    ## Command communication to the server
        elif self.ms.lower() == "exit":
            self.s.send(self.ms.encode('utf-8'))
            self.s.close()
            subprocess.run(['pkill', 'chat.py'])  
        elif self.ms.lower() == "!list":
            self.s.send(self.ms.encode('utf-8'))
        elif self.ms[:4].lower() == "!add":
            self.s.send(self.ms.encode('utf-8'))
        elif self.ms[:7].lower() == "!admins":
            self.s.send(self.ms.encode('utf-8'))
        elif self.ms[:5].lower() == "!kick":
            self.s.send(self.ms.encode('utf-8'))
        elif self.ms[:7].lower() == "!remove":
            self.s.send(self.ms.encode('utf-8'))
        elif self.ms[:1] == "@":
            a = self.ms.split()[0]
            self.ms = self.ms + " [From " + self.name + "]\n"
            self.ms = self.ms.replace(a, '')
            self.encrypt(self.ms.encode('utf-8'))
            self.ms = a + ' ' + self.encrypted
            self.s.send(self.ms.encode('utf-8'))
        elif self.ms[:5].lower() == "!help":
            self.s.send(self.ms.encode('utf-8'))
        else:
    ## Send message to the server (encrypt and send -- in the sending function)
            self.ms= BC.G + self.name + BC.E + ": " + self.ms
            self.sending(self.ms)

    def initing(self):
    ## Thread for receiving messages
        Thread(target=self.threaded_user, args=(self.s,)).start()
    ## Send username to the server
        self.s.send(self.name.encode('utf-8'))
        sleep(1)
        self.s.send(self.password.encode('utf-8'))
        sleep(1)
    ## Commands to show at initial connection made
        self.s.send('!list'.encode('utf-8'))
        sleep(.5)
        self.s.send('!admins'.encode('utf-8'))
        while True:
            self.main()
        ServerSocket.close()



